let start = document.getElementById('start');
const cards = document.querySelectorAll(".card");


let flipped = false;
let lock = false;
let card1, card2;
let moveCount = 0;
let totalFlips = 0;
start.addEventListener('click', doStart);

function doStart(){
    
  let timeCount = 0;
  let timer = setInterval(() => {
  timeCount++;
  document.getElementById('time-count').textContent = timeCount;
  }, 1000);

  start.disabled = true;
  start.style.color = 'plum';
}

function flipCard(e) {
  moveCount++;
  document.getElementById('moves-count').textContent = moveCount;
  if (lock) {
    return;
  }
  
  if (e.target.parentElement === card1) {
    return;
  } 

  e.target.parentElement.classList.add("flip");

  if (!flipped) {
    flipped = true;
    card1 = e.target.parentElement;
    totalFlips++;
    return;
  }

  card2 = e.target.parentElement;
  totalFlips++;
  match();
}

function match() {
 
  if(card1.dataset.cards === card2.dataset.cards) {
    console.log(card1.dataset.cards, card2.dataset.cards);
    yesMatch();
  } else {
    console.log(card1.dataset.cards, card2.dataset.cards);
    noMatch();
  }
  
} 

function yesMatch() {
  card1.removeEventListener("click", flipCard);
  card2.removeEventListener("click", flipCard);

  if (totalFlips === 16) {
    restart();
  }

  resetFlips();
}

function noMatch() {
  lock = true;
  totalFlips -= 2;

  setTimeout(() => {
    card1.classList.remove("flip");
    card2.classList.remove("flip");

    resetFlips();
  }, 1000);
}

function resetFlips() {
  [flipped, lock] = [false, false];
  [card1, card2] = [null, null];
}

function restart() {
  location.reload();
}

function shuffle() {
  cards.forEach((card) => {
    let randomPos = Math.floor(Math.random() * 16);
    card.style.order = randomPos;
  });
}; 

shuffle();

cards.forEach((card) => card.addEventListener("click", flipCard));
